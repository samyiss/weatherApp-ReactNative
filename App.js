import { StyleSheet, Text, View, Image } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import React, { useEffect, useState } from 'react';

function heure_format(time) {
    const date = new Date(time * 1000);
    const hours = date.getHours();
    const minutes = "0" + date.getMinutes();
    const seconds = "0" + date.getSeconds();
    const formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    return formattedTime;
}

export default function App() {
    const [temp, setTemp] = useState(0);
    const [temp_min, setTempMin] = useState(0);
    const [temp_max, setTempMax] = useState(0);
    const [sunrise, setSunrise] = useState(0);
    const [sunset, setSunset] = useState(0);

    const [imgURL, setImgURL] = useState('http://openweathermap.org/img/wn/04n@2x.png');

    const [ville, setVille] = useState('Montreal,CA');

    useEffect( () => {
        async function getData() {
            const url = `https://api.openweathermap.org/data/2.5/weather?q=${ville}&units=metric&appid=70394fa076117f64fef603fdfef793ff`
            let rep = await fetch(url);
            if (rep.ok) {
                let data = await rep.json();
                setSunset(heure_format(data.sys.sunset));
                setSunrise(heure_format(data.sys.sunrise));
                setTemp(data.main.temp);
                setTempMin(data.main.temp_min);
                setTempMax(data.main.temp_max);
                setImgURL(`http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`);
            }
        }
        getData();
    }, [ville]);

    return (
        <View style={styles.container}>

            <Text style={styles.title}>Temperature</Text>

            <Picker
                style={{ height: 25, width: 200 }}

                selectedValue={ville}
                onValueChange={(itemValue) => setVille(itemValue)}
                >
                <Picker.Item label="Montreal,CA" value="Montreal,CA" />
                <Picker.Item label="Trois-Rivières,CA" value="Trois-Rivières,CA" />
                <Picker.Item label="Québec,CA" value="Québec,CA" />
                <Picker.Item label="Ottawa,CA" value="Ottawa,CA" />
                <Picker.Item label="Toronto,CA" value="Toronto,CA" />
                <Picker.Item label="Vancouver,CA" value="Vancouver,CA" />
                <Picker.Item label="Edmonton,CA" value="Edmonton,CA" />
                <Picker.Item label="San Francisco,US" value="San Francisco,US" />
                <Picker.Item label="New York City,US" value="New York City,US" />
                <Picker.Item label="Washington,US" value="Washington,US" />
            </Picker>

            <View style={{height: 20}}/>
            
            <Text>Température Actuelle (temp): {temp} </Text>
            <Text>Température min (temp_min): {temp_min} </Text>
            <Text>Température max (temp_max): {temp_max} </Text>
            <Text>Lever du soleil: {sunrise} </Text>
            <Text>Coucher du soleil: {sunset} </Text>
            <Image 
                style={{width: 100, height: 100}}
                source = {{uri: imgURL }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
        title: {
        paddingVertical: 8,
        borderRadius: 6,
        color: 'blue',
        textAlign: "center",
        fontSize: 25,
        fontWeight: "bold"
    }
});
